import { app, BrowserWindow, screen, dialog, ipcMain } from 'electron';
import * as path from 'path';
import * as url from 'url';
import { autoUpdater } from 'electron-updater';

let win, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === '--serve');

const log = require('electron-log');
let downloadProgress: number;
log.transports.file.level = "debug";
autoUpdater.logger = log;

function sendStatusToWindow(text) {
  log.info(text);
  win.webContents.send('message', text);
}

function sendNoUpdateToWindow(text) {
  log.info(text);
  win.webContents.send('no-update', text);
}

function sendUpdatingToWindow(text) {
  log.info(text);
  win.webContents.send('updating', text);
}

//Buscar actualizaciones
autoUpdater.on('checking-for-update', () => {    
  sendStatusToWindow('Buscando actualizaciones...');
});

autoUpdater.on('update-available', (ev, info) => {    
  sendStatusToWindow('Actualización disponible.');
});

autoUpdater.on('update-not-available', (ev, info) => {    
  sendNoUpdateToWindow('No hay actualizaciones disponibles.');
});

autoUpdater.on('download-progress', (progressObj) => {
  /*let log_message = "Velocidad de descarga: " + progressObj.bytesPerSecond;
  log_message = log_message + ' - Descargado ' + progressObj.percent.toFixed() + '%';
  log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';*/
  sendUpdatingToWindow(progressObj.percent.toFixed());
});

autoUpdater.on('update-downloaded', (ev, info) => {
  sendStatusToWindow('Actualización descargada; se instalará en 5 segundos.');
  setTimeout(function() {
    autoUpdater.quitAndInstall();
  }, 5000);
});

function createWindow() {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width,
    height: size.height,
    icon: __dirname + '/assets/icons/64x64.png',
    fullscreen: true,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  if (serve) {
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL('http://localhost:4200');
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }
  
  //Quitar barra de menu
  win.removeMenu();
  //win.webContents.openDevTools();


  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  autoUpdater.checkForUpdates();

}

try {

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  app.on('ready', () => {
    createWindow();
  });

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}
