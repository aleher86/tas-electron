import { autoUpdater } from 'electron-updater';
import * as path from 'path';
import * as url from 'url';
import { dialog, BrowserWindow, ipcMain } from 'electron';

const log = require('electron-log');
let downloadProgress: number;
log.transports.file.level = "debug";
autoUpdater.logger = log;

autoUpdater.autoDownload = false;

export function check() {
    autoUpdater.checkForUpdates();

    autoUpdater.on('checking-for-update', () => {
        dialog.showMessageBox({
            type: 'info',
            title: 'Actualización disponible',
            message: 'Hay una nueva actualización de la aplicación. ¿Quiere actualizar ahora?',
            buttons: ['Actualizar', 'No']
        }, (index) => {
            if (index) {
                return;
            } else {
                autoUpdater.downloadUpdate();
    
                let proWin = new BrowserWindow({
                    width: 350,
                    height: 35,
                    useContentSize: true,
                    autoHideMenuBar: true,
                    maximizable: false,
                    fullscreen: false,
                    fullscreenable: false,
                    resizable: false,
                    title: 'Descargando actualización'
                });
    
                proWin.loadURL(url.format({
                    pathname: path.join(__dirname, 'dist/progress.html'),
                    protocol: 'file:',
                    slashes: true
                }));
    
                proWin.on('closed', () => {
                    proWin = null;
                });
    
                ipcMain.on('download-progress-request', (e) => {
                    e.returnValue = downloadProgress;
                });
    
                autoUpdater.on('download-progress', (d) => {
                    downloadProgress = d.percent;
                    proWin.webContents.send('message', downloadProgress);
                    autoUpdater.logger.info(downloadProgress);
                });
    
                autoUpdater.on('update-downloaded', () => {
                    //if (progressWindow) progressWindow.close();
    
                    dialog.showMessageBox({
                        type: 'info',
                        title: 'Actualizacion lista',
                        message: 'Hay preparada una nueva actualización de la aplicación. ¿Salir e instalar ahora?',
                        buttons: ['Sí', 'Más tarde']
                    }, (index) => {
                        if (!index) {
                            autoUpdater.quitAndInstall();
                        }
                    });
                });
            }
        });
    });
};
