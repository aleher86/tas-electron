import { Component} from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { ConnectionService } from 'ng-connection-service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  public isConnected = true;
  public apiReachable = true;

  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    private connectionService: ConnectionService
  ) {
    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (electronService.isElectron) {

      this.connectionService.monitor().subscribe(isConnected => {
        this.isConnected = isConnected;
      });

      console.log(process.env);
      console.log('Mode electron');
      console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }
  }
}
