import 'reflect-metadata';
import '../polyfills';

import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared/shared.module';
import localeEsAr from '@angular/common/locales/es-AR';
import { registerLocaleData, DatePipe } from '@angular/common';

//import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//import { HomeModule } from './home/home.module';

//componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { PanelPartidaComponent } from './components/shared/panel-partida/panel-partida.component';
import { TasaServicioGeneralComponent } from './components/tasa-servicio-general/tasa-servicio-general.component';
import { TasaServicioGeneralDetalleComponent } from './components/tasa-servicio-general-detalle/tasa-servicio-general-detalle.component';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { TasasComponent } from './components/tasas/tasas.component';
import { TasaServicioGeneralMenuComponent } from './components/tasa-servicio-general-menu/tasa-servicio-general-menu.component';
import { TasaServicioGeneralDeudaComponent } from './components/tasa-servicio-general-deuda/tasa-servicio-general-deuda.component';
import { TasaServicioGeneralDeudaDetalleComponent } from './components/tasa-servicio-general-deuda-detalle/tasa-servicio-general-deuda-detalle.component';
import { PatentesMotoComponent } from './components/patentes-moto/patentes-moto.component';
import { PatentesMotoDetalleComponent } from './components/patentes-moto-detalle/patentes-moto-detalle.component';
import { PatentesAutoComponent } from './components/patentes-auto/patentes-auto.component';
import { PatentesAutoDetalleComponent } from './components/patentes-auto-detalle/patentes-auto-detalle.component';
import { ErrorPlantillaComponent } from './components/shared/error-plantilla/error-plantilla.component';

//servicios
import { TasasService } from './services/tasas.service';
import { ImpresoraService } from './services/impresora.service';

//rutas
import { ROUTES } from './app.routes';

registerLocaleData(localeEsAr);

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    PanelPartidaComponent,
    TasaServicioGeneralComponent,
    TasaServicioGeneralDetalleComponent,
    LoadingComponent,
    TasasComponent,
    TasaServicioGeneralMenuComponent,
    TasaServicioGeneralDeudaComponent,
    TasaServicioGeneralDeudaDetalleComponent,
    PatentesMotoComponent,
    PatentesMotoDetalleComponent,
    PatentesAutoComponent,
    PatentesAutoDetalleComponent,
    ErrorPlantillaComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    RouterModule.forRoot( ROUTES, { useHash: true } ),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    TasasService,
    ImpresoraService,
    DatePipe,
    { provide: LOCALE_ID, useValue: 'es-Ar' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
