import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TasaServicioGeneralComponent } from './components/tasa-servicio-general/tasa-servicio-general.component';
import { TasaServicioGeneralDetalleComponent } from './components/tasa-servicio-general-detalle/tasa-servicio-general-detalle.component';
import { TasasComponent } from './components/tasas/tasas.component';
import { TasaServicioGeneralMenuComponent } from './components/tasa-servicio-general-menu/tasa-servicio-general-menu.component';
import { TasaServicioGeneralDeudaComponent } from './components/tasa-servicio-general-deuda/tasa-servicio-general-deuda.component';
import { TasaServicioGeneralDeudaDetalleComponent } from './components/tasa-servicio-general-deuda-detalle/tasa-servicio-general-deuda-detalle.component';
import { PatentesMotoComponent } from './components/patentes-moto/patentes-moto.component';
import { PatentesMotoDetalleComponent } from './components/patentes-moto-detalle/patentes-moto-detalle.component';
import { PatentesAutoComponent } from './components/patentes-auto/patentes-auto.component';
import { PatentesAutoDetalleComponent } from './components/patentes-auto-detalle/patentes-auto-detalle.component';


export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'tasas', component: TasasComponent },
    { path: 'tasa-servicio-general', component: TasaServicioGeneralMenuComponent },
    { path: 'tasa-servicio-general/deuda', component: TasaServicioGeneralDeudaComponent },
    { path: 'tasa-servicio-general/detalle-deuda/:identificador', component: TasaServicioGeneralDeudaDetalleComponent },
    { path: 'tasa-servicio-general/actual', component: TasaServicioGeneralComponent },
    { path: 'tasa-servicio-general/detalle/:identificador', component: TasaServicioGeneralDetalleComponent },
    { path: 'patentes-motos', component: PatentesMotoComponent },
    { path: 'patentes-motos/detalle/:identificador', component: PatentesMotoDetalleComponent },
    { path: 'patentes-autos', component: PatentesAutoComponent },
    { path: 'patentes-autos/detalle/:identificador', component: PatentesAutoDetalleComponent },
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' },
  ];
