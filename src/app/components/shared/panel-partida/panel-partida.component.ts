import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-panel-partida',
  templateUrl: './panel-partida.component.html'
})
export class PanelPartidaComponent implements OnInit {
  @Output() numeroSeleccionado: EventEmitter<string>;
  @Output() activarBorrar: EventEmitter<any>;

  constructor() {
    this.numeroSeleccionado = new EventEmitter();
    this.activarBorrar = new EventEmitter();
  }

  ngOnInit() {
  }

  getInput(inputAdded: string) {
    this.numeroSeleccionado.emit( inputAdded );
  }

  borrar() {
    this.activarBorrar.emit();
  }

}
