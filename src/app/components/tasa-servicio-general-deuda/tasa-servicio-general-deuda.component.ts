import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tasa-servicio-general-deuda',
  templateUrl: './tasa-servicio-general-deuda.component.html'
})
export class TasaServicioGeneralDeudaComponent implements OnInit {

  public display: string = '';
  
  constructor( private router: Router ) { }

  ngOnInit() {

  }

  updateInput( inputAdded: string ) {
    this.display += inputAdded;
  }

  deleteInput() {
    this.display = this.display.substring(0, this.display.length - 1);
  }

  verDetalle( id: any ) {
    this.router.navigate( ['/tasa-servicio-general/detalle-deuda', id] );
  }

}
