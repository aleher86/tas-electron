import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { TasasService } from '../../services/tasas.service';
import { ImpresoraService } from '../../services/impresora.service';

@Component({
  selector: 'app-tasa-servicio-general-detalle',
  templateUrl: './tasa-servicio-general-detalle.component.html'
})
export class TasaServicioGeneralDetalleComponent implements OnInit {

  public cuenta: any = {};
  public deuda: any = {};
  public loading: boolean;
  public month: any = new Date().getMonth() + 1;
  public year: any = new Date().getFullYear();
  public foundCuenta: boolean;
  public foundDeuda: boolean;
  public error: boolean;
  public fechaHoy: any;
  public lastDay: any;
  public vencimiento: any;
  public vencimiento2: any;

  constructor( private router: Router, private routerActivated: ActivatedRoute, private tasa: TasasService, private printer: ImpresoraService) {
    this.loading = true;
    this.foundCuenta = false;
    this.foundDeuda = false;
    this.error = false;
    this.fechaHoy = new Date();
    this.lastDay = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);

    this.routerActivated.params.subscribe( params => {
      this.getCuenta(params['identificador']);
     });
  }

  ngOnInit() {
    /*setTimeout(() => {
      this.router.navigate(['home']);
    }, 50000);*/
  }

  getCuenta( id: string ) {
    this.loading = true;

    this.tasa.getCuenta( id, '1' ).subscribe( ( cuenta: any ) => {
      if(cuenta){
        this.cuenta = cuenta;
        this.foundCuenta = true;
        console.log(cuenta);
        this.getDeuda(this.cuenta.id, this.month, this.year);
      }else{
        this.loading = false;
        console.log("not found cuenta");
      }
    },
    error => {
      console.log(error);
      this.error = error;
      this.loading = false;
    }
    );

  }

  getDeuda( cuenta: any, periodo: any, anio: any ) {
    this.loading = true;
    this.tasa.getDeuda( cuenta, periodo, anio ).subscribe( ( deuda: any ) => {
      if (deuda) {
        this.deuda = deuda;
        this.vencimiento = new Date(deuda.fechaVencimiento);
        this.vencimiento2 = new Date(deuda.fechaVencimiento2);
        this.loading = false;
        this.foundDeuda = true;
        console.log(this.deuda);
      } else {
        this.loading = false;
      }
    });
  }

  imprimir(cuenta: any, deuda: any) {
    this.printer.printTSG(cuenta, deuda);
    this.router.navigate( ['/home'] );
  }

}
