import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { TasasService } from '../../services/tasas.service';
import { ImpresoraService } from '../../services/impresora.service';

@Component({
  selector: 'app-tasa-servicio-general-deuda-detalle',
  templateUrl: './tasa-servicio-general-deuda-detalle.component.html'
})
export class TasaServicioGeneralDeudaDetalleComponent implements OnInit {
  
  public cuenta: any = {};
  public deudas: any = {};
  public loading: boolean;
  public month: any = new Date().getMonth() + 1;
  public year: any = new Date().getFullYear();
  public foundCuenta: boolean;
  public foundDeuda: boolean;
  public error: boolean;

  constructor( private router: Router, private routerActivated: ActivatedRoute, private tasa: TasasService, private printer: ImpresoraService ) { 
    this.loading = true;
    this.foundCuenta = false;
    this.foundDeuda = false;
    this.error = false;

    this.routerActivated.params.subscribe( params => {
      this.getCuenta(params['identificador']);
     });
  }

  ngOnInit() {
  }

  getCuenta( id: string ) {
    this.loading = true;

    this.tasa.getCuenta( id, '1' ).subscribe( ( cuenta: any ) => {
      if(cuenta){
        this.cuenta = cuenta;
        this.foundCuenta = true;
        //console.log(cuenta);
        this.getDeudas(this.cuenta.id);
      }else{
        this.loading = false;
        //console.log("not found cuenta");
      }
    },
    error => {
      //console.log(error);
      this.error = error;
      this.loading = false;
    });

  }

  getDeudas( cuenta: any) {
    this.loading = true;
    this.tasa.getDeudas( cuenta ).subscribe( ( deuda: any ) => {
      if(deuda) {
        this.deudas = deuda;
        this.loading = false;
        this.foundDeuda = true;
        //console.log(this.deudas);
      } else {
        this.loading = false;
        //console.log("no deuda");
      }
    });
  }

  imprimir(cuenta: any, deudas: any) {

    let totalDeuda = 0;

    deudas.forEach( deuda => {
      totalDeuda = totalDeuda + deuda.saldo;
      deudas['total'] = totalDeuda;
    });
    this.printer.printTSGDeudas(cuenta, deudas);
    this.router.navigate( ['/home'] );
  }

}
