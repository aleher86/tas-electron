import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { TasasService } from '../../services/tasas.service';
import { ImpresoraService } from '../../services/impresora.service';

@Component({
  selector: 'app-patentes-auto-detalle',
  templateUrl: './patentes-auto-detalle.component.html'
})
export class PatentesAutoDetalleComponent implements OnInit {

  public cuenta: any = {};
  public deuda: any = {};
  public loading: boolean;
  public month: any = new Date().getMonth() + 1;
  public year: any = new Date().getFullYear();
  public foundCuenta: boolean;
  public foundDeuda: boolean;
  public error: boolean;

  constructor( private router: Router, private routerActivated: ActivatedRoute, private tasa: TasasService, private printer: ImpresoraService ) { 
    this.loading = true;
    this.foundCuenta = false;
    this.foundDeuda = false;
    this.error = false;

    this.routerActivated.params.subscribe( params => {
      this.getCuenta(params['identificador']);
     });
  }

  ngOnInit() {
  }

  getCuenta( id: string ) {
    this.loading = true;

    this.tasa.getCuenta( id, '11' ).subscribe( ( cuenta: any ) => {
      if(cuenta){
        this.cuenta = cuenta;
        this.foundCuenta = true;
        //console.log(cuenta);
        this.getDeuda(this.cuenta.id);
      }else{
        this.loading = false;
        //console.log("not found cuenta");
      }
    },
    error => {
      //console.log(error);
      this.error = error;
      this.loading = false;
    });

  }

  getDeuda( cuenta: any) {
    this.loading = true;
    this.tasa.getDeudaAutos( cuenta ).subscribe( ( deuda: any ) => {
      if(deuda){
        this.deuda = deuda;
        this.loading = false;
        this.foundDeuda = true;
        console.log(this.deuda);
      }else{
        this.loading = false;
        //console.log("no deuda");
      }
    });
  }

  imprimir(cuenta: any, deuda: any) {
    this.printer.printAuto(cuenta, deuda);
    this.router.navigate( ['/home'] );
  }

}
