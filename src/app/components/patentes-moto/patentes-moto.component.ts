import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import Keyboard from "simple-keyboard";

@Component({
  selector: 'app-patentes-moto',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './patentes-moto.component.html',
  styleUrls: [
    '../../../../node_modules/simple-keyboard/build/css/index.css'
  ]
})

export class PatentesMotoComponent implements OnInit {

  public value = '';
  public keyboard: Keyboard;

  ngAfterViewInit() {
    this.keyboard = new Keyboard({
      /**
       * Layout by:
       * Sterling Butters (https://github.com/SterlingButters)
       */
      layout: {
        default: [
          "1 2 3 4 5 6 7 8 9 0",
          "Q W E R T Y U I O P",
          "A S D F G H J K L",
          "Z X C V B N M {backspace}",
        ],
        shift: [
          "1 2 3 4 5 6 7 8 9 0",
          "Q W E R T Y U I O P",
          "A S D F G H J K L",
          "Z X C V B N M {backspace}",
        ]
      },
      display: {
        "{backspace}": "Borrar ⌫",
      },
      onChange: input => this.onChange(input),
      onKeyPress: button => this.onKeyPress(button)
    });
  }

  onChange = (input: string) => {
    this.value = input;
    //console.log("Input changed", input);
  }

  onKeyPress = (button: string) => {
    //console.log("Button pressed", button);

    /**
     * If you want to handle the shift and caps lock buttons
     */
    if (button === "{shift}" || button === "{lock}") {
      this.handleShift();
    }
  }

  onInputChange = (event: any) => {
    this.keyboard.setInput(event.target.value);
  };

  handleShift = () => {
    let currentLayout = this.keyboard.options.layoutName;
    let shiftToggle = currentLayout === "default" ? "shift" : "default";

    this.keyboard.setOptions({
      layoutName: shiftToggle
    });
  };


  constructor( private router: Router ) { }

  ngOnInit() {
  }

  verDetalle( id: any ) {
    this.router.navigate( ['/patentes-motos/detalle', id] );
  }

}
