import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tasa-servicio-general',
  templateUrl: './tasa-servicio-general.component.html'
})
export class TasaServicioGeneralComponent implements OnInit {

  public display: string = '';

  constructor( private router: Router ) { }

  ngOnInit() {

  }

  updateInput( inputAdded: string ) {
    this.display += inputAdded;
  }

  deleteInput() {
    this.display = this.display.substring(0, this.display.length - 1);
  }

  verDetalle( id: any ) {
    this.router.navigate( ['/tasa-servicio-general/detalle', id] );
  }

}
