import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
const ThermalPrinter = window.require("node-thermal-printer").printer;
const PrinterTypes = window.require("node-thermal-printer").types;

@Injectable({
  providedIn: 'root'
})
export class ImpresoraService {

  public fecha = new Date();

  constructor( private datePipe: DatePipe) { }

  getPrinter() {

    const printer = new ThermalPrinter({
      type: PrinterTypes.EPSON,
      interface: 'printer:CUSTOM TG2480-H',
      driver: window.require('printer'),
      characterSet: 'PC858_EURO',
    });

    return printer;

  }

  printTSG( titular: any, periodo: any) {
    const printer = this.getPrinter();
    const fecha = new Date();
    const vencimiento = new Date(periodo.fechaVencimiento);
    const vencimiento2 = new Date(periodo.fechaVencimiento2);
    const fechaHoy = this.datePipe.transform(fecha, 'dd/MM/yyyy h:mm a');

    printer.alignCenter();

    /*printer.printImage(__dirname + '/assets/img/logo-brown.png').then(() => {

    }).catch((err) => {
      console.log(err);
    });*/

    printer.bold(true);
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('Tasa de Servicios Generales');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Partida: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    if ( vencimiento > fecha ) {
      printer.println('Fecha Vto.: ' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy')  );
    } else if ( fecha < vencimiento2 ) {
      printer.println('Fecha Vto.: ' + this.datePipe.transform(periodo.fechaVencimiento2, 'dd/MM/yyyy')  );
    } else {
      const date = new Date();
      const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      printer.println('Fecha Vto.: ' + this.datePipe.transform(lastDay, 'dd/MM/yyyy')  );
    }
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    printer.table([periodo.periodo + '/' + periodo.anio, '$' + periodo.importe]);
    if (periodo.actualizacion) {
      printer.table(['Recargo:', '$' + periodo.actualizacion]);
    }
    printer.table(['Total:', '$' + periodo.saldo]);
    printer.newLine();
    printer.bold(false);
    printer.newLine();
    printer.alignCenter();
    printer.printBarcode('{A' + periodo.codbarShort, 73);
    printer.newLine();
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.alignCenter();
    printer.println('-------- CORTAR --------');
    printer.newLine();
    printer.bold(true);
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('DUPLICADO');
    printer.println('Tasa de Servicios Generales');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Partida: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    if ( vencimiento > fecha ) {
      printer.println('Fecha Vto.: ' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy')  );
    } else if ( fecha < vencimiento2 ) {
      printer.println('Fecha Vto.: ' + this.datePipe.transform(periodo.fechaVencimiento2, 'dd/MM/yyyy')  );
    } else {
      const date = new Date();
      const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      printer.println('Fecha Vto.: ' + this.datePipe.transform(lastDay, 'dd/MM/yyyy')  );
    }
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    printer.table([periodo.periodo + '/' + periodo.anio, '$' + periodo.importe]);
    if (periodo.actualizacion) {
      printer.table(['Recargo:', '$' + periodo.actualizacion]);
    }
    printer.table(['Total:', '$' + periodo.saldo]);
    printer.newLine();
    printer.bold(false);
    printer.newLine();
    printer.alignCenter();
    printer.printBarcode('{A' + periodo.codbarShort, 73);
    printer.newLine();
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.cut();
    printer.execute();
  }

  printTSGDeudas( titular: any, deudas: any) {
    const printer = this.getPrinter();
    const fecha = new Date();
    const fechaHoy = this.datePipe.transform(fecha, 'dd/MM/yyyy h:mm a');

    let totalDeuda = 0;

    printer.alignCenter();
    /*printer.printImage('https://tas.brown.gob.ar/img/logo-brown.png').then(() => {

    }).catch((err) => {
      console.log(err);
    });*/
    printer.bold(true);
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('Tasa de Servicios Generales');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Partida: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    printer.newLine();
    printer.alignCenter();
    printer.println('Deudas');
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    deudas.forEach( deuda => {
      printer.table([deuda.periodo + '/' + deuda.anio, '$' + deuda.saldo]);
    });
    printer.table(['Total', '$' + deudas.total]);
    printer.newLine();
    printer.alignCenter();
    printer.bold(false);
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696 (int. 226)');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.cut();
    printer.execute();
  }


  printMoto( titular: any, periodo: any) {
    const printer = this.getPrinter();
    const fecha = new Date();
    const fechaHoy = this.datePipe.transform(fecha, 'dd/MM/yyyy h:mm a');

    printer.alignCenter();

    /*printer.printImage(__dirname + '/assets/img/logo-brown.png').then(() => {

    }).catch((err) => {
      console.log(err);
    });*/

    printer.bold(true);
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('Patentes de rodados');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Patente: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    printer.println('Fecha de vencimiento: ' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy'));
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    printer.table([periodo.periodo + '/' + periodo.anio, '$' + periodo.saldo]);
    printer.newLine();
    printer.bold(false);
    printer.println('Código solo válido hasta' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy'));
    printer.newLine();
    printer.alignCenter();
    printer.printBarcode('{A' + periodo.barCodeShortCobol, 73);
    printer.newLine();
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696 (int. 226)');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.alignCenter();
    printer.println('-------- CORTAR --------');
    printer.newLine();
    printer.bold(true);
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('DUPLICADO');
    printer.println('Patentes de rodados');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Patente: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    printer.println('Fecha de vencimiento: ' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy'));
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    printer.table([periodo.periodo + '/' + periodo.anio, '$' + periodo.saldo]);
    printer.newLine();
    printer.bold(false);
    printer.println('Código solo válido hasta' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy'));
    printer.newLine();
    printer.alignCenter();
    printer.printBarcode('{A' + periodo.barCodeShortCobol, 73);
    printer.newLine();
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696 (int. 226)');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.cut();
    printer.execute();
  }

  printAuto( titular: any, periodo: any) {
    const printer = this.getPrinter();
    const fecha = new Date();
    const fechaHoy = this.datePipe.transform(fecha, 'dd/MM/yyyy h:mm a');

    printer.alignCenter();

    /*printer.printImage(__dirname + '/assets/img/logo-brown.png').then(() => {

    }).catch((err) => {
      console.log(err);
    });*/

    printer.bold(true);
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('Impuesto Automotor');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Patente: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    printer.println('Fecha de vencimiento: ' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy')  );
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    printer.table([periodo.periodo + '/' + periodo.anio, '$' + periodo.saldo]);
    printer.newLine();
    printer.bold(false);
    printer.println('Código solo válido hasta' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy'));
    printer.newLine();
    printer.alignCenter();
    printer.printBarcode('{A' + periodo.barCodeShortCobol, 73);
    printer.newLine();
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696 (int. 226)');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.alignCenter();
    printer.println('-------- CORTAR --------');
    printer.newLine();
    printer.println('Municipalidad de Almirante Brown');
    printer.newLine();
    printer.println('Impuesto Automotor');
    printer.alignLeft();
    printer.newLine();
    printer.println('Fecha y hora: ' + fechaHoy);
    printer.println('Patente: ' + titular.identificador);
    printer.println('Titular: ' + titular.titular);
    printer.println('Domicilio: ' + titular.domicilio);
    printer.println('Fecha de vencimiento: ' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy')  );
    printer.newLine();
    printer.table(['Periodo', 'Importe']);
    printer.table([periodo.periodo + '/' + periodo.anio, '$' + periodo.saldo]);
    printer.newLine();
    printer.bold(false);
    printer.println('Código solo válido hasta' + this.datePipe.transform(periodo.fechaVencimiento, 'dd/MM/yyyy'));
    printer.newLine();
    printer.alignCenter();
    printer.printBarcode('{A' + periodo.barCodeShortCobol, 73);
    printer.newLine();
    printer.println('Gracias por su consulta.');
    printer.println('Para dudas y consultas puede comunicarse');
    printer.println('a la Agencia Municipal de Recaudación');
    printer.println('llamando al 0800-222-7696 (int. 226)');
    printer.println('de 08:00 a 14:00 hs.');
    printer.newLine();
    printer.cut();
    printer.execute();
  }

  printTestBarCode(){
    const printer = this.getPrinter();
    printer.println('probando code');
    printer.printBarcode('{A44541881929000000612', 73);
    printer.newLine();
    printer.cut();
    printer.execute();
  }

}
