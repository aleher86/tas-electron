import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { format } from 'path';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class TasasService {

  constructor( private http: HttpClient, private datePipe: DatePipe ) {
  }

  getQuery( query: string ) {
    const URL = `https://api.brown.gob.ar${query}`;

    return this.http.get(URL);
 }

 getCuenta( identificador: string, recurso: string ) {
  if ( recurso === '1') {
    identificador = identificador.replace(/^0+/, '');
  }

  return this.getQuery(`/cuentas.json?identificador=${ identificador }&recurso=${ recurso }`).pipe( map( data => data['0'] ));
 }

 getDeuda( cuenta: any, periodo: any, anio: any ) {
  return this.getQuery(`/deudas.json?cuenta=${ cuenta }&periodo=${ periodo }&anio=${ anio }`).pipe( map( data => data['0'] ));
 }

 getDeudaMotos( cuenta: any) {
  const fecha = new Date();
  const fechaHoy = this.datePipe.transform(fecha, 'yyyy-MM-dd');

  return this.getQuery(`/deudas.json?cuenta=${ cuenta }&fecha_vencimiento[strictly_after]=${fechaHoy}`).pipe( map( data => data['0'] ));
 }

 getDeudaAutos( cuenta: any) {
  const fecha = new Date();
  const fechaHoy = this.datePipe.transform(fecha, 'yyyy-MM-dd');

  return this.getQuery(`/deudas.json?cuenta=${ cuenta }&fecha_vencimiento[strictly_after]=${fechaHoy}&clasificacion=1`).pipe( map( data => data['0'] ));
 }

 getDeudas( cuenta: any ) {
  const fecha = new Date();
  const fechaHoy = this.datePipe.transform(fecha, 'yyyy-MM-dd');
  
  return this.getQuery(`/deudas.json?cuenta=${ cuenta }&fecha_vencimiento[strictly_before]=${fechaHoy}`).pipe( map( data => data ));
 }

}
