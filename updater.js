"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_updater_1 = require("electron-updater");
var path = require("path");
var url = require("url");
var electron_1 = require("electron");
var log = require('electron-log');
var downloadProgress;
log.transports.file.level = "debug";
electron_updater_1.autoUpdater.logger = log;
electron_updater_1.autoUpdater.autoDownload = false;
function check() {
    electron_updater_1.autoUpdater.checkForUpdates();
    electron_updater_1.autoUpdater.on('checking-for-update', function () {
        electron_1.dialog.showMessageBox({
            type: 'info',
            title: 'Actualización disponible',
            message: 'Hay una nueva actualización de la aplicación. ¿Quiere actualizar ahora?',
            buttons: ['Actualizar', 'No']
        }, function (index) {
            if (index) {
                return;
            }
            else {
                electron_updater_1.autoUpdater.downloadUpdate();
                var proWin_1 = new electron_1.BrowserWindow({
                    width: 350,
                    height: 35,
                    useContentSize: true,
                    autoHideMenuBar: true,
                    maximizable: false,
                    fullscreen: false,
                    fullscreenable: false,
                    resizable: false,
                    title: 'Descargando actualización'
                });
                proWin_1.loadURL(url.format({
                    pathname: path.join(__dirname, 'dist/progress.html'),
                    protocol: 'file:',
                    slashes: true
                }));
                proWin_1.on('closed', function () {
                    proWin_1 = null;
                });
                electron_1.ipcMain.on('download-progress-request', function (e) {
                    e.returnValue = downloadProgress;
                });
                electron_updater_1.autoUpdater.on('download-progress', function (d) {
                    downloadProgress = d.percent;
                    proWin_1.webContents.send('message', downloadProgress);
                    electron_updater_1.autoUpdater.logger.info(downloadProgress);
                });
                electron_updater_1.autoUpdater.on('update-downloaded', function () {
                    //if (progressWindow) progressWindow.close();
                    electron_1.dialog.showMessageBox({
                        type: 'info',
                        title: 'Actualizacion lista',
                        message: 'Hay preparada una nueva actualización de la aplicación. ¿Salir e instalar ahora?',
                        buttons: ['Sí', 'Más tarde']
                    }, function (index) {
                        if (!index) {
                            electron_updater_1.autoUpdater.quitAndInstall();
                        }
                    });
                });
            }
        });
    });
}
exports.check = check;
;
//# sourceMappingURL=updater.js.map